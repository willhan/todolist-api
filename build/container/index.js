"use strict";

var _tsyringe = require("tsyringe");

var _repositore = require("../modules/task/repositore");

var _UsersRepository = require("../modules/user/repositore/UsersRepository");

_tsyringe.container.registerSingleton('UsersRepository', _UsersRepository.UserRepository);

_tsyringe.container.registerSingleton('TaskRepository', _repositore.TaskRepository);