"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateUser1659400831632 = void 0;

var _typeorm = require("typeorm");

class CreateUser1659400831632 {
  async up(queryRunner) {
    await queryRunner.createTable(new _typeorm.Table({
      name: 'users',
      columns: [{
        name: 'id',
        type: 'uuid',
        isPrimary: true
      }, {
        name: 'name',
        type: 'varchar'
      }, {
        name: 'email',
        type: 'varchar',
        isUnique: true
      }, {
        name: 'nickname',
        type: 'varchar',
        isUnique: true
      }, {
        name: 'created_at',
        type: 'timestamp',
        default: 'now()'
      }, {
        name: 'updated_at',
        type: 'timestamp',
        default: 'now()'
      }]
    }));
  }

  async down(queryRunner) {
    await queryRunner.dropTable('users');
  }

}

exports.CreateUser1659400831632 = CreateUser1659400831632;