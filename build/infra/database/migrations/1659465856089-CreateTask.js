"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateTask1659465856089 = void 0;

var _typeorm = require("typeorm");

class CreateTask1659465856089 {
  async up(queryRunner) {
    await queryRunner.createTable(new _typeorm.Table({
      name: 'tasks',
      columns: [{
        name: 'id',
        type: 'uuid',
        isPrimary: true
      }, {
        name: 'title',
        type: 'varchar'
      }, {
        name: 'user_id',
        type: 'uuid'
      }, {
        name: 'description',
        type: 'varchar'
      }, {
        name: 'limit_date',
        type: 'varchar'
      }, {
        name: 'created_at',
        type: 'timestamp',
        default: 'now()'
      }, {
        name: 'updated_at',
        type: 'timestamp',
        default: 'now()'
      }],
      foreignKeys: [{
        name: 'taskByUser',
        referencedTableName: 'users',
        referencedColumnNames: ['id'],
        columnNames: ['user_id'],
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      }]
    }));
  }

  async down(queryRunner) {
    await queryRunner.dropTable('tasks');
  }

}

exports.CreateTask1659465856089 = CreateTask1659465856089;