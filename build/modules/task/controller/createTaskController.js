"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classTransformer = require("class-transformer");

var _tsyringe = require("tsyringe");

var _createTaskService = _interopRequireDefault(require("../service/createTaskService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class CreateTaskControllers {
  async create(req, res) {
    const createSessionService = _tsyringe.container.resolve(_createTaskService.default);

    const {
      user_id,
      title,
      description,
      limit_date
    } = req.body;
    const createTask = await createSessionService.create({
      user_id,
      title,
      description,
      limit_date
    });
    return res.status(201).json((0, _classTransformer.instanceToInstance)(createTask));
  }

}

var _default = CreateTaskControllers;
exports.default = _default;