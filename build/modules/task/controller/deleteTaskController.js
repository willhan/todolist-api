"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _deleteTaskService = _interopRequireDefault(require("../service/deleteTaskService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class DeleteTaskControllers {
  async delete(req, res) {
    const user = _tsyringe.container.resolve(_deleteTaskService.default);

    const {
      id
    } = req.params;
    await user.delete(id);
    return res.status(200).json({
      message: 'Task removed successfully'
    });
  }

}

var _default = DeleteTaskControllers;
exports.default = _default;