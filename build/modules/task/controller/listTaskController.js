"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classTransformer = require("class-transformer");

var _tsyringe = require("tsyringe");

var _listTaskService = _interopRequireDefault(require("../service/listTaskService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class ListTaskControllers {
  async list(req, res) {
    const user = _tsyringe.container.resolve(_listTaskService.default);

    const page = req.query.page ? Number(req.query.page) : 1;
    const limit = req.query.limit ? Number(req.query.limit) : 15;
    const products = await user.list({
      page,
      limit
    });
    return res.status(200).json((0, _classTransformer.instanceToInstance)(products));
  }

  async listById(req, res) {
    const user = _tsyringe.container.resolve(_listTaskService.default);

    const {
      id
    } = req.params;
    const productById = await user.listById(id);
    return res.status(200).json((0, _classTransformer.instanceToInstance)(productById));
  }

}

var _default = ListTaskControllers;
exports.default = _default;