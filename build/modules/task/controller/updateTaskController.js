"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classTransformer = require("class-transformer");

var _tsyringe = require("tsyringe");

var _updateTaskService = _interopRequireDefault(require("../service/updateTaskService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class UpdateTaskControllers {
  async update(req, res) {
    const user = _tsyringe.container.resolve(_updateTaskService.default);

    const {
      user_id,
      title,
      description,
      limit_date
    } = req.body;
    const {
      id
    } = req.params;
    const updateTask = await user.update({
      id,
      user_id,
      title,
      description,
      limit_date
    });
    return res.json((0, _classTransformer.instanceToInstance)(updateTask));
  }

}

var _default = UpdateTaskControllers;
exports.default = _default;