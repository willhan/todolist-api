"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskRepository = void 0;

var _typeorm = require("typeorm");

var _taskEntitie = _interopRequireDefault(require("../../../infra/entities/taskEntitie"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class TaskRepository {
  constructor() {
    _defineProperty(this, "ormRepository", void 0);

    this.ormRepository = (0, _typeorm.getRepository)(_taskEntitie.default);
  }

  async findByTitle(title) {
    const taskTitle = await this.ormRepository.findOne({
      title
    });
    return taskTitle;
  }

  async create(task) {
    const createTask = this.ormRepository.create(task);
    await this.ormRepository.save(createTask);
    return createTask;
  }

  async save(task) {
    await this.ormRepository.save(task);
    return task;
  }

  async remove(task) {
    await this.ormRepository.remove(task);
  }

  async findAll({
    page,
    skip,
    take
  }) {
    const [tasks, count] = await this.ormRepository.createQueryBuilder().skip(skip).take(take).getManyAndCount();
    const result = {
      per_page: take,
      total: count,
      current_page: page,
      data: tasks
    };
    return result;
  }

  async findByName(title) {
    const task = await this.ormRepository.findOne({
      title
    });
    return task;
  }

  async findById(id) {
    const task = await this.ormRepository.findOne({
      id
    });
    return task;
  }

}

exports.TaskRepository = TaskRepository;