"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _createTaskController = _interopRequireDefault(require("../controller/createTaskController"));

var _deleteTaskController = _interopRequireDefault(require("../controller/deleteTaskController"));

var _listTaskController = _interopRequireDefault(require("../controller/listTaskController"));

var _updateTaskController = _interopRequireDefault(require("../controller/updateTaskController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const createTaskController = new _createTaskController.default();
const listTaskController = new _listTaskController.default();
const updateTaskController = new _updateTaskController.default();
const deleteTaskController = new _deleteTaskController.default();
const task = (0, _express.Router)();
task.post('/', createTaskController.create);
task.get('/', listTaskController.list);
task.get('/:id', listTaskController.listById);
task.put('/:id', updateTaskController.update);
task.delete('/:id', deleteTaskController.delete);
var _default = task;
exports.default = _default;