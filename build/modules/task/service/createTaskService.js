"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _error = _interopRequireDefault(require("../../../utils/error"));

var _interface = require("../interface");

var _dec, _dec2, _dec3, _dec4, _class;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let CreateTaskService = (_dec = (0, _tsyringe.injectable)(), _dec2 = function (target, key) {
  return (0, _tsyringe.inject)('TaskRepository')(target, undefined, 0);
}, _dec3 = Reflect.metadata("design:type", Function), _dec4 = Reflect.metadata("design:paramtypes", [typeof _interface.ITaskRepository === "undefined" ? Object : _interface.ITaskRepository]), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = class CreateTaskService {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
  }

  async create({
    user_id,
    title,
    description,
    limit_date
  }) {
    const taskExists = await this.taskRepository.findByName(title);

    if (taskExists) {
      throw new _error.default('There is already one task with this name');
    }

    const product = await this.taskRepository.create({
      user_id,
      title,
      description,
      limit_date
    });
    return product;
  }

}) || _class) || _class) || _class) || _class);
var _default = CreateTaskService;
exports.default = _default;