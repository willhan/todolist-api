"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _error = _interopRequireDefault(require("../../../utils/error"));

var _interface = require("../interface");

var _dec, _dec2, _dec3, _dec4, _class;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let ListTaskService = (_dec = (0, _tsyringe.injectable)(), _dec2 = function (target, key) {
  return (0, _tsyringe.inject)('TaskRepository')(target, undefined, 0);
}, _dec3 = Reflect.metadata("design:type", Function), _dec4 = Reflect.metadata("design:paramtypes", [typeof _interface.ITaskRepository === "undefined" ? Object : _interface.ITaskRepository]), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = class ListTaskService {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
  }

  async list({
    page,
    limit
  }) {
    const take = limit;
    const skip = (Number(page) - 1) * take;
    const listUsers = await this.taskRepository.findAll({
      page,
      skip,
      take
    });
    return listUsers;
  }

  async listById(id) {
    const listUsers = await this.taskRepository.findById(id);

    if (!listUsers) {
      throw new _error.default('User not found', 404);
    }

    return listUsers;
  }

}) || _class) || _class) || _class) || _class);
var _default = ListTaskService;
exports.default = _default;