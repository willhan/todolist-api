"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _error = _interopRequireDefault(require("../../../utils/error"));

var _interface = require("../interface");

var _dec, _dec2, _dec3, _dec4, _class;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let UpdateTaskService = (_dec = (0, _tsyringe.injectable)(), _dec2 = function (target, key) {
  return (0, _tsyringe.inject)('TaskRepository')(target, undefined, 0);
}, _dec3 = Reflect.metadata("design:type", Function), _dec4 = Reflect.metadata("design:paramtypes", [typeof _interface.ITaskRepository === "undefined" ? Object : _interface.ITaskRepository]), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = class UpdateTaskService {
  constructor(taskRepository) {
    this.taskRepository = taskRepository;
  }

  async update({
    id,
    user_id,
    title,
    description,
    limit_date
  }) {
    const titleExists = await this.taskRepository.findByTitle(title);
    const updateTask = await this.taskRepository.findById(id);
    console.log(titleExists);
    console.log(updateTask);

    if (!updateTask) {
      throw new _error.default('Task not found', 404);
    }

    if (titleExists && title !== updateTask.title) {
      throw new _error.default('There is already one product with this email', 409);
    }

    updateTask.title = title;
    updateTask.description = description;
    updateTask.limit_date = limit_date;
    updateTask.user_id = user_id;
    await this.taskRepository.save(updateTask);
    return updateTask;
  }

}) || _class) || _class) || _class) || _class);
var _default = UpdateTaskService;
exports.default = _default;