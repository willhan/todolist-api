"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _deleteUserService = _interopRequireDefault(require("../service/deleteUserService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class DeleteUsersControllers {
  async delete(req, res) {
    const user = _tsyringe.container.resolve(_deleteUserService.default);

    const {
      id
    } = req.params;
    await user.delete(id);
    return res.status(200).json({
      message: 'user removed successfully'
    });
  }

}

var _default = DeleteUsersControllers;
exports.default = _default;