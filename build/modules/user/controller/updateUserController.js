"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _classTransformer = require("class-transformer");

var _tsyringe = require("tsyringe");

var _UpdateUserService = _interopRequireDefault(require("../service/UpdateUserService"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class UpdateUsersControllers {
  async update(req, res) {
    const user = _tsyringe.container.resolve(_UpdateUserService.default);

    const {
      name,
      email,
      nickname
    } = req.body;
    const {
      id
    } = req.params;
    const updateProduct = await user.update({
      id,
      name,
      email,
      nickname
    });
    return res.json((0, _classTransformer.instanceToInstance)(updateProduct));
  }

}

var _default = UpdateUsersControllers;
exports.default = _default;