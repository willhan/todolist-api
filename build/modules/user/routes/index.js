"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _createUserController = _interopRequireDefault(require("../controller/createUserController"));

var _deleteUserController = _interopRequireDefault(require("../controller/deleteUserController"));

var _listUserController = _interopRequireDefault(require("../controller/listUserController"));

var _updateUserController = _interopRequireDefault(require("../controller/updateUserController"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const listUsersController = new _listUserController.default();
const createUsersController = new _createUserController.default();
const updateUsersController = new _updateUserController.default();
const deleteUsersController = new _deleteUserController.default();
const users = (0, _express.Router)();
users.get('/', listUsersController.list);
users.get('/:id', listUsersController.listById);
users.post('/', createUsersController.create);
users.put('/:id', updateUsersController.update);
users.delete('/:id', deleteUsersController.delete);
var _default = users;
exports.default = _default;