"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tsyringe = require("tsyringe");

var _error = _interopRequireDefault(require("../../../utils/error"));

var _IUsers = require("../interface/IUsers");

var _dec, _dec2, _dec3, _dec4, _class;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let CreateUserService = (_dec = (0, _tsyringe.injectable)(), _dec2 = function (target, key) {
  return (0, _tsyringe.inject)('UsersRepository')(target, undefined, 0);
}, _dec3 = Reflect.metadata("design:type", Function), _dec4 = Reflect.metadata("design:paramtypes", [typeof _IUsers.IUsersRepository === "undefined" ? Object : _IUsers.IUsersRepository]), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = class CreateUserService {
  constructor(usersRepository) {
    this.usersRepository = usersRepository;
  }

  async create({
    name,
    email,
    nickname
  }) {
    const emailExists = await this.usersRepository.findByEmail(email);

    if (emailExists) {
      throw new _error.default('email address already used');
    }

    const user = this.usersRepository.create({
      name,
      email,
      nickname
    });
    return user;
  }

}) || _class) || _class) || _class) || _class);
var _default = CreateUserService;
exports.default = _default;