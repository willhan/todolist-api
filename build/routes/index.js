"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _swagger = _interopRequireDefault(require("../swagger.json"));

var _swaggerUiExpress = _interopRequireDefault(require("swagger-ui-express"));

var _routes = _interopRequireDefault(require("../modules/user/routes"));

var _routes2 = _interopRequireDefault(require("../modules/task/routes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = (0, _express.Router)();
routes.get('/', (req, res) => {
  res.json('Hello Dev!! Welcome To Do List API by Willhan Marques, please use routes BASE_URL/todo-doc');
});
routes.use('/users', _routes.default);
routes.use('/task', _routes2.default);
routes.use('/todo-doc', _swaggerUiExpress.default.serve, _swaggerUiExpress.default.setup(_swagger.default));
var _default = routes;
exports.default = _default;